# AMES ENGLISH PROJECT

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/doanthuc12/ames-english-project.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/doanthuc12/ames-english-project/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

---

# GIT

## Install

https://git-scm.com/downloads

- Check cài đăt thành công:

* Window: open cmd -> git -v

MAC:

- open terminal
- Install brew: https://docs.brew.sh/Installation
- Install git: brew install git
- Check git: git -v

## Create repository github

- Login: https://github.com/
- Tạo 1 repository

## Clone repository

- Open VScode
- Open folder code
- Open terminal vscode tại folder code
- Nhập: git clone <HTTPS của repository>

* Lưu ý: Cần phải kiểm soát chặt chẽ cấu trúc folder code.

- Repo bản thân tự tạo: dùng để code (code nhớ FORMAT)
- Repo document: Yêu cầu không thay đổi code. Chỉ dùng git pull để lấy tài liệu.

## Step git push code

- Lần đầu:

* Khởi tạo git: git init
* Config email : git config --global user.email '<email>'
* Config username : git config --global user.name '<name>'
* Add toàn bộ file change: git add .
* Tạo commit mô tả: git commit -m 'homework1'
* git push

---

# GITLAB

Khởi tạo git: git init

- Config email : git config --global user.email '<email>'
- Config username : git config --global user.name '<name>'
- Add toàn bộ file change: git add .
- Tạo commit mô tả: git commit -m 'homework1'
- git remote add origin https://gitlab.com/doanthuc12/ames-english-project
- git push origin master

* Những lần sau:

- Add toàn bộ file change: git add .
- Tạo commit mô tả: git commit -m 'homework1'
- git push

## Run HTML file:

- MAC: open index.html
- Window: start index.html

* NOTE:

- Có thể chọn RUN -> START (F5)
- Open folder -> click double vào tên file html.

#ICON

- Chèn link source: <script src="https://kit.fontawesome.com/042e59ddf7.js" crossorigin="anonymous"></script>
- Web: fontawesome.com

#ANIMATION

- C1: CHÈN LINK TRỰC TIẾP VÀO <HEAD>: https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css
- C2: Mở link đã chèn trong C1 --> copy path --> tạo file ANIMATION.MIN.CSS --> dán path vào và link với file HTML

---

# Getting Started with Create React App (https://create-react-app.dev/docs/adding-typescript/)

npx create-react-app my-app --template typescript

# Install

**Language**
I18n (https://react.i18next.com/guides/quick-start) npm install react-i18next i18next --save

**Framework**
Bootstrap (https://getbootstrap.com/docs/5.0/getting-started/introduction/) npm install bootstrap
React_Bootstrap (https://react-bootstrap.netlify.app/getting-started/introduction/) npm install react-bootstrap bootstrap

**Package**
React Router (https://reactrouter.com/en/main) npm install react-router-dom

**Redux**
(https://redux.js.org/introduction/getting-started)

**Redux toolkit**
(https://redux-toolkit.js.org/usage/usage-with-typescript)
npm install redux react-redux @reduxjs/toolkit @types/redux-logger
npm i redux-thunk

Note: Toolkit useAppDispatch & useAppSelector
Declare export type RootState = ReturnType; export type AppDispatch = typeof store.dispatch; type TypedDispatch = ThunkDispatch<T, any, AnyAction>; export const useAppDispatch = () => useDispatch<TypedDispatch>(); export const useAppSelector: TypedUseSelectorHook = useSelector;
Usage const dispatch = useAppDispatch(); const state = useAppSelector((state: AppState) => state.xxx);
