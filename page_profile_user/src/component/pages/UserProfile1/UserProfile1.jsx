import React, { useEffect, useState } from "react";
import axios from "axios";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import "bootstrap/dist/css/bootstrap.min.css";
import "./UserProfile1.css";
import Bg_header from "../../../img/bg_header.jpg";
import Bg_body from "../../../img/bg_body.jpg";
// import users from "../../../data/users.json";
import { TbBan } from "react-icons/tb";
import { AiFillEdit } from "react-icons/ai";

import IncomeTable from "../Features/IncomeTable/IncomeTable";
import OlympicTable from "../Features/OlympicTable/OlympicTable";
import OlympicTable1 from "../Features/OlympicTable/OlympicTable1";
import ResultTable from "../Features/ResultTable/ResultTable";
import ResultTable1 from "../Features/ResultTable/ResultTable1";

const cdnServerUrl = "https://server.sachso.edu.vn/api/v1.0/dynamic/query";
const authorizationKey = "Basic 12C1F7EF9AC8E288FBC2177B7F54D";

export default function UserProfile1() {
  const [users, setUsers] = useState([]);
  console.log("🚀 ~ file: UserProfile1.jsx:15 ~ UserProfile1 ~ users:", users);

  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  const [startDate, setStartDate] = useState(null);

  const [totalCourses, setTotalCourses] = useState(0);
  const [totalCompletedCourses, setTotalCompletedCourses] = useState(0);

  // Thêm hàm tính tổng số khoá học
  function calculateTotalCourses(usersData) {
    let totalCourses = 0;
    usersData.forEach((user) => {
      // Đổi tên thuộc tính "revenue" thành thuộc tính thích hợp trong dữ liệu của bạn
      totalCourses += user.revenue.length;
    });
    return totalCourses;
  }

  useEffect(() => {
    axios({
      headers: {
        "Content-Type": "application/json",
        Authorization: authorizationKey,
        ApplicationName: "Ebm",
      },
      method: "post",
      url: cdnServerUrl,
      data: {
        sqlCommand: "p_EBM_Student_Profile_Get_v2",
        parameters: {
          // StudentId: 11544998,
          StudentId: 11979755,
        },
      },
    })
      .then((response) => {
        // Set the users state with the data
        setUsers(response.data.results[0].profile); // Assuming the data structure matches your expectations
      })
      .catch((error) => {
        console.log("error", error);
      });
  }, []); // Chỉ chạy một lần khi component được tạo

  // Tính tổng totalCourses và totalCompletedCourses từ dữ liệu người dùng
  useEffect(() => {
    // Hàm tính tổng số khoá học từ dữ liệu người dùng
    function calculateTotalCourses(usersData) {
      let totalCourses = 0;
      usersData.forEach((user) => {
        // Đổi tên thuộc tính "revenue" thành thuộc tính thích hợp trong dữ liệu của bạn
        totalCourses += user.revenue;
      });
      return totalCourses;
    }

    // Hàm tính tổng số khoá học đã hoàn thành từ dữ liệu người dùng
    function calculateTotalCompletedCourses(usersData) {
      let totalCompletedCourses = 0;
      usersData.forEach((user) => {
        // Đổi tên thuộc tính "completedCourses" thành thuộc tính thích hợp trong dữ liệu của bạn
        totalCompletedCourses += user.completedCourses;
      });
      return totalCompletedCourses;
    }

    // Tính tổng totalCourses và totalCompletedCourses từ dữ liệu người dùng
    const calculatedTotalCourses = calculateTotalCourses(users);
    setTotalCourses(calculatedTotalCourses);

    // Tương tự, tính tổng số khoá học đã hoàn thành nếu có
    const calculatedTotalCompletedCourses =
      calculateTotalCompletedCourses(users);
    setTotalCompletedCourses(calculatedTotalCompletedCourses);
  }, [users]); // Chạy lại effect khi users thay đổi

  return (
    <div className="main-content">
      {/* Top navbar */}
      <nav
        className="navbar navbar-top navbar-expand-md navbar-dark"
        id="navbar-main"
      >
        <div className="container-fluid">
          {/* Brand */}
          <span
            className="heading-label h4 mb-0 text-white text-uppercase d-none d-lg-inline-block"

            // href="https://www.creative-tim.com/product/argon-dashboard"
            // target="_blank"
          >
            TRANG CÁ NHÂN
          </span>
          {/* Form */}
          <form className="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
            {/* <div className="form-group mb-0">
                <div className="input-group input-group-alternative">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-search"></i>
                    </span>
                  </div>
                  <input
                    className="form-control"
                    placeholder="Search"
                    type="text"
                  />
                </div>
              </div> */}
          </form>
          {/* User */}
          {users.length > 0 ? (
            users.map((users) => (
              <ul
                key={users.id}
                className="navbar-nav align-items-center d-none d-md-flex"
              >
                <li className="nav-item dropdown">
                  <a
                    className="nav-link pr-0"
                    href="#"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <div className="media align-items-center">
                      <span className="avatar avatar-sm rounded-circle">
                        <a href="#">
                          <img
                            src={
                              users.avatar ||
                              "https://iupac.org/wp-content/uploads/2018/05/default-avatar.png"
                            }
                            className="rounded-circle"
                            alt="Profile"
                          />
                        </a>
                      </span>
                      <div className="media-body ml-2 d-none d-lg-block">
                        <span className="mb-0 text-sm font-weight-bold text-white">
                          {users.lastname}
                        </span>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            ))
          ) : (
            <p>Loading...</p> // You can show a loading message while fetching data
          )}
        </div>
      </nav>
      {/* Header */}
      {users.map((users) => (
        <div
          className="header pb-6 pt-lg-4 d-flex align-items-center"
          style={{
            minHeight: "200px",
            backgroundImage: `url(${Bg_header})`,
            //backgroundImage: `url:"https://vietnamteachingjobs.com/wp-content/uploads/wpjobboard/company/797/company-logo/logo_AMES-ENGLISH_2018-01.png"`,
            backgroundSize: "contain",
            backgroundPosition: "top",
          }}
        >
          <span className="mask bg-gradient-default opacity-8"></span>
          {/* Header container */}
          <div className="container-fluid d-flex align-items-center">
            <div className="row">
              <div className="col-lg-7 col-md-10">
                {/* <h1 className="display-2 text-white">Xin chào</h1> */}
                {/* <p className="text-white mt-0 mb-5"></p> */}
                {/* <a href="#!" className="btn btn-info">
                  Edit profile
                </a> */}
              </div>
            </div>
          </div>
        </div>
      ))}

      {/* Page content */}
      {users.length > 0 ? (
        users.map((users) => (
          <div
            key={users.id}
            className="container-fluid mt--7"
            // style={{
            //   // minHeight: "200px",
            //   backgroundImage: `url(${Bg_body})`,
            //   //backgroundImage: `url:"https://vietnamteachingjobs.com/wp-content/uploads/wpjobboard/company/797/company-logo/logo_AMES-ENGLISH_2018-01.png"`,
            //   backgroundSize: "contain",
            //   backgroundPosition: "top",
            //   paddingBottom: "50px",
            // }}
          >
            <div className="row">
              <div className="col-xl-12 order-xl-2 mb-2 mb-xl-0">
                <div className="card card-profile shadow">
                  <div className="row justify-content-center">
                    <div className="col-lg-3 order-lg-2">
                      <div className="card-profile-image">
                        <a href="#">
                          <img
                            src={
                              users.avatar ||
                              "https://iupac.org/wp-content/uploads/2018/05/default-avatar.png"
                            }
                            className="rounded-circle"
                            alt="Profile"
                          />
                        </a>
                        <button
                          className="edit-button"
                          style={{
                            position: "relative",
                            top: "106px",
                            left: "50px",
                            backgroundColor: "transparent",

                            border: "none",
                          }}
                          onClick={(e) => {
                            e.preventDefault();
                            // Add your edit functionality here
                          }}
                          title="Cập nhật ảnh đại diện"
                        >
                          <AiFillEdit />{" "}
                        </button>
                      </div>
                    </div>
                  </div>
                  {/* <div className="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div className="d-flex justify-content-between">
                      <a href="#" className="btn btn-sm btn-info mr-4">
                        Connect
                      </a>
                      <a
                        href="#"
                        className="btn btn-sm btn-default float-right"
                      >
                        Message
                      </a>
                    </div>
                  </div> */}
                  <br />
                  <div className="card-body pt-0 pt-md-4">
                    <div className="row mb-4">
                      <div className="col">
                        <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                          <div>
                            <span className="heading">
                              {totalCourses !== undefined &&
                              totalCourses !== null
                                ? totalCourses
                                : "Trống"}
                            </span>
                            <span className="description">
                              Khoá học tham gia
                            </span>
                          </div>
                          <div>
                            <span className="heading">
                              {totalCompletedCourses !== undefined &&
                              totalCompletedCourses !== null
                                ? totalCompletedCourses
                                : "Trống"}
                            </span>
                            <span className="description">
                              Khoá học đã hoàn thành
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="text-center">
                      <h3>
                        {users.firstname}
                        {/* <span className="font-weight-light">
                          , lớp {users.grade}
                        </span> */}
                      </h3>

                      <div className="h4 mt-4">
                        <i className="ni business_briefcase-24 mr-2"></i>
                        Học viên - Lớp {users.grade}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              {/* THÔNG TIN NGƯỜI DÙNG */}
              <div className="col-xl-12 order-xl-1">
                <div className="card bg-secondary shadow">
                  <div className="card-header bg-blue border-0">
                    <div className="row align-items-left">
                      <div className="col">
                        <h3 className="mb-0 text-left heading">
                          THÔNG TIN NGƯỜI DÙNG
                        </h3>
                      </div>
                    </div>
                  </div>

                  {/* DETAIL PROFILE */}
                  <div className="card-body">
                    <form>
                      {/* Thông tin người dùng */}
                      <h6 className="heading-medium text-muted mb-4">
                        THÔNG TIN CÁ NHÂN
                      </h6>
                      <div className="pl-lg-4">
                        <div className="row">
                          {/* Email*/}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left">
                              <label
                                className="form-control-label col-xl-4"
                                htmlFor="input-email"
                              >
                                <span className="required">*</span>Email (dùng
                                để đăng nhập)
                              </label>
                              <input
                                type="email"
                                id="disabledInput"
                                className="form-control form-control-alternative col-xl-8"
                                placeholder="Email"
                                value={users.email}
                                readOnly
                                disabled
                                onMouseEnter={handleMouseEnter}
                                onMouseLeave={handleMouseLeave}
                              />
                              {isHovered && <TbBan className="ban-icon" />}{" "}
                              {/* Hiển thị biểu tượng dấu bằng màu đỏ khi hover */}
                            </div>
                          </div>
                          {/* SĐT */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label col-xl-4"
                                htmlFor="input-phone"
                              >
                                Số điện thoại
                              </label>
                              <input
                                type="text"
                                id={`input-phone-${users.id}`}
                                className="form-control form-control-alternative col-xl-8"
                                placeholder="Số điện thoại"
                                value={users.phone}
                                // onChange={(e) =>
                                //   this.handleUpdate(
                                //     users.id,
                                //     "phone",
                                //     e.target.value
                                //   )
                                // }
                              />
                            </div>
                          </div>
                        </div>

                        <div className="row">
                          {/* Họ và tên đệm */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label col-xl-4"
                                htmlFor="input-username"
                              >
                                Họ và tên đệm
                              </label>
                              <input
                                type="text"
                                id="input-lastname"
                                className="form-control form-control-alternative col-xl-8"
                                placeholder="Họ và tên"
                                value={users.lastname}
                              />
                            </div>
                          </div>
                          {/* Tên */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label col-xl-4"
                                htmlFor="input-username"
                              >
                                Tên
                              </label>
                              <input
                                type="text"
                                id="input-lastname"
                                className="form-control form-control-alternative col-xl-8"
                                placeholder="Họ và tên"
                                value={users.lastname}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="row">
                          {/* Giới tính */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label col-xl-4"
                                htmlFor="input-gender"
                              >
                                Giới tính
                              </label>
                              <select
                                className="form-control form-control-alternative form-select col-xl-8"
                                id="input-gender"
                                aria-label="Default select example"
                                placeholder="Giới tính"
                                value={users.gender || ""}
                              >
                                {!users.gender && (
                                  <option value="">
                                    {/* Default option text */}Chọn giới tính
                                  </option>
                                )}
                                <option value="1">Nữ</option>
                                <option value="2">Nam</option>
                              </select>
                            </div>
                          </div>
                          {/* ngày sinh */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label col-xl-4"
                                htmlFor="input-dob"
                              >
                                Ngày sinh
                              </label>

                              <div className="custom-datepicker-input">
                                <DatePicker
                                  selected={startDate}
                                  // onChange={(date) => setStartDate(date)}
                                  id="input-dob"
                                  value={users.dob}
                                  className="form-control form-control-alternative"
                                  placeholderText="Chọn ngày sinh"
                                  dateFormat="dd/MM/yyyy"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* Thông tin liên lạc */}
                      <h6 className="heading-medium text-muted mb-4">
                        THÔNG TIN LIÊN LẠC
                      </h6>
                      <div className="pl-lg-4">
                        <div className="row">
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label col-xl-4"
                                htmlFor="input-username"
                              >
                                Họ và tên (Bố/Mẹ)
                              </label>
                              <input
                                type="text"
                                id="input-parentName"
                                className="form-control form-control-alternative col-xl-8"
                                placeholder="Họ và tên (Bố/Mẹ)"
                                value={users.parentName}
                              />
                            </div>
                          </div>
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label col-xl-4"
                                htmlFor="input-phone"
                              >
                                Số điện thoại
                              </label>
                              <input
                                type="text"
                                id="input-phone"
                                className="form-control form-control-alternative col-xl-8"
                                placeholder="Số điện thoại"
                                value={users.parentPhone}
                              />
                            </div>
                          </div>
                          <div className="col text-right">
                            <button
                              className="btn btn-sm btn-primary"
                              // onClick={this.handleUpdate}
                            >
                              Cập nhật
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <br />
              {/* TỔNG HỢP KẾT QUẢ HỌC TẬP */}
              <div className="col-xl-12 order-xl-2">
                <div className="card bg-secondary shadow">
                  <div className="card-header bg-blue border-0">
                    <div className="row align-items-center">
                      <div className="col">
                        <h3 className="mb-0 text-left heading">
                          TỔNG HỢP KẾT QUẢ HỌC TẬP
                        </h3>
                      </div>
                    </div>
                  </div>

                  {/* STUDY RESULT TABLE */}
                  <div className="card-body">
                    <form>
                      {/* Kết quả học tập */}
                      <h6 className="heading-medium text-muted mb-4">
                        CÁC KHOÁ HỌC ĐÃ THAM GIA
                      </h6>
                      <div className="pl-lg-4">
                        <div>
                          <ResultTable />
                        </div>
                      </div>
                    </form>
                  </div>

                  {/* OLYMPIC TABLE */}
                  <div className="card-body">
                    <form>
                      <h6 className="heading-medium text-muted mb-4">
                        THAM GIA OLYMPIC
                      </h6>
                      <div className="pl-lg-4">
                        <div>
                          <OlympicTable1 />
                        </div>
                      </div>
                    </form>
                  </div>

                  {/* INCOME TABLE */}
                  <div className="card-body">
                    <form>
                      <h6 className="heading-medium text-muted mb-4">
                        DOANH THU ĐÓNG GÓP
                      </h6>
                      <div className="pl-lg-4">
                        <div>
                          <IncomeTable />
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
}
