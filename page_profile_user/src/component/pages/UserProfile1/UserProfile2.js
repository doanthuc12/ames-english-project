import React, { Component, useEffect } from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import "./UserProfile1.css";
// import users from "../../../data/users.json";

import IncomeTable from "../Features/IncomeTable/IncomeTable";
import OlympicTable from "../Features/OlympicTable/OlympicTable";
import ResultTable from "../Features/ResultTable/ResultTable";

class UserProfile extends Component {
  constructor() {
    super();
    this.state = {
      users: [],

      error: null,
      totalCourses: 0, // Thêm state để lưu tổng khoá học
      totalRevenue: 0, // Thêm state để lưu tổng doanh thu
    };
  }

  componentDidMount() {
    fetch("../../../data/users.json")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        console.log(data);
        const totalCourses = this.calculateTotalCourses(data);
        const totalRevenue = this.calculateTotalRevenue(data);

        this.setState({ users: data, error: null, totalCourses, totalRevenue });
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
        this.setState({ error: "Error fetching data" });
      });
  }

  // Hàm tính tổng khoá học từ dữ liệu người dùng
  calculateTotalCourses(usersData) {
    let totalCourses = 0;
    usersData.forEach((user) => {
      totalCourses += user.revenue.length;
    });
    return totalCourses;
  }

  // Hàm tính tổng doanh thu từ dữ liệu người dùng
  calculateTotalRevenue(usersData) {
    let totalRevenue = 0;
    usersData.forEach((user) => {
      user.revenue.forEach((item) => {
        totalRevenue += parseFloat(item.money);
      });
    });
    return totalRevenue;
  }

  handleUpdate = (userId, updatedField, updatedValue) => {
    // Create a copy of the users array
    const updatedUsers = [...this.state.users];

    // Find the user by their ID
    const userIndex = updatedUsers.findIndex((user) => user.id === userId);

    // Update the specific field of the user
    updatedUsers[userIndex][updatedField] = updatedValue;

    // Update the state
    this.setState({ users: updatedUsers });
  };

  render() {
    // const { users } = this.state;
    const { users, totalCourses, totalRevenue } = this.state;

    return (
      <div className="main-content">
        {/* Top navbar */}
        <nav
          className="navbar navbar-top navbar-expand-md navbar-dark"
          id="navbar-main"
        >
          <div className="container-fluid">
            {/* Brand */}
            <span
              className="heading-label h4 mb-0 text-white text-uppercase d-none d-lg-inline-block"
              // href="https://www.creative-tim.com/product/argon-dashboard"
              // target="_blank"
            >
              TRANG CÁ NHÂN
            </span>
            {/* Form */}
            <form className="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
              {/* <div className="form-group mb-0">
                <div className="input-group input-group-alternative">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-search"></i>
                    </span>
                  </div>
                  <input
                    className="form-control"
                    placeholder="Search"
                    type="text"
                  />
                </div>
              </div> */}
            </form>
            {/* User */}
            {users.map((user) => (
              <ul
                key={user.id}
                className="navbar-nav align-items-center d-none d-md-flex"
              >
                <li className="nav-item dropdown">
                  <a
                    className="nav-link pr-0"
                    href="#"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <div className="media align-items-center">
                      <span className="avatar avatar-sm rounded-circle">
                        <a href="#">
                          <img
                            src={user.avatar}
                            className="rounded-circle"
                            alt="Profile"
                          />
                        </a>
                      </span>
                      <div className="media-body ml-2 d-none d-lg-block">
                        <span className="mb-0 text-sm font-weight-bold text-white">
                          {user.name}
                        </span>
                      </div>
                    </div>
                  </a>
                  {/* <div className="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div className="dropdown-header noti-title">
                      <h6 className="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a
                      href="../examples/profile.html"
                      className="dropdown-item"
                    >
                      <i className="ni ni-single-02"></i>
                      <span>My profile</span>
                    </a>
                    <a
                      href="../examples/profile.html"
                      className="dropdown-item"
                    >
                      <i className="ni ni-settings-gear-65"></i>
                      <span>Settings</span>
                    </a>
                    <a
                      href="../examples/profile.html"
                      className="dropdown-item"
                    >
                      <i className="ni ni-calendar-grid-58"></i>
                      <span>Activity</span>
                    </a>
                    <a
                      href="../examples/profile.html"
                      className="dropdown-item"
                    >
                      <i className="ni ni-support-16"></i>
                      <span>Support</span>
                    </a>
                    <div className="dropdown-divider"></div>
                    <a href="#!" className="dropdown-item">
                      <i className="ni ni-user-run"></i>
                      <span>Logout</span>
                    </a>
                  </div> */}
                </li>
              </ul>
            ))}
          </div>
        </nav>
        {/* Header */}
        <div
          className="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
          style={{
            minHeight: "200px",
            backgroundImage:
              "url(https://browsecat.art/sites/default/files/royal-blue-background-128935-379206-9247253.png)",
            backgroundSize: "cover",
            backgroundPosition: "center top",
          }}
        >
          <span className="mask bg-gradient-default opacity-8"></span>
          {/* Header container */}
          {/* <div className="container-fluid d-flex align-items-center">
            <div className="row">
              <div className="col-lg-7 col-md-10">
                <h1 className="display-2 text-white">Hello Jesse</h1>
                <p className="text-white mt-0 mb-5">
                  This is your profile page. You can see the progress you've
                  made with your work and manage your projects or assigned tasks
                </p>
                <a href="#!" className="btn btn-info">
                  Edit profile
                </a>
              </div>
            </div>
          </div> */}
        </div>

        {/* Page content */}
        {users.map((user) => (
          <div key={user.id} className="container-fluid mt--7">
            <div className="row">
              <div className="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                <div className="card card-profile shadow">
                  <div className="row justify-content-center">
                    <div className="col-lg-3 order-lg-2">
                      <div className="card-profile-image">
                        <a href="#">
                          <img
                            src={user.avatar}
                            className="rounded-circle"
                            alt="Profile"
                          />
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* <div className="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div className="d-flex justify-content-between">
                      <a href="#" className="btn btn-sm btn-info mr-4">
                        Connect
                      </a>
                      <a
                        href="#"
                        className="btn btn-sm btn-default float-right"
                      >
                        Message
                      </a>
                    </div>
                  </div> */}
                  <div className="card-body pt-0 pt-md-4">
                    <div className="row">
                      <div className="col">
                        <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                          <div>
                            <span className="heading">{totalCourses}</span>
                            <span className="description">
                              Khoá học tham gia
                            </span>
                          </div>
                          <div>
                            <span className="heading">{totalCourses}</span>
                            <span className="description">
                              Khoá học đã hoàn thành
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="text-center">
                      <h3>
                        {user.name}
                        {/* <span className="font-weight-light">
                          , lớp {user.grade}
                        </span> */}
                      </h3>

                      <div className="h5 mt-4">
                        <i className="ni business_briefcase-24 mr-2"></i>
                        {user.role} - Lớp {user.grade}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* THÔNG TIN NGƯỜI DÙNG */}
              <div className="col-xl-8 order-xl-1">
                <div className="card bg-secondary shadow">
                  <div className="card-header bg-white border-0">
                    <div className="row align-items-left">
                      <div className="col">
                        <h3 className="mb-0 text-left heading">
                          THÔNG TIN NGƯỜI DÙNG
                        </h3>
                      </div>
                    </div>
                  </div>

                  {/* DETAIL PROFILE */}
                  <div className="card-body">
                    <form>
                      {/* Thông tin người dùng */}
                      <h6 className="heading-medium text-muted mb-4">
                        THÔNG TIN CÁ NHÂN
                      </h6>
                      <div className="pl-lg-4">
                        <div className="row">
                          {/* Email*/}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left">
                              <label
                                className="form-control-label"
                                htmlFor="input-email"
                              >
                                Email (dùng để đăng nhập)
                              </label>
                              <input
                                type="email"
                                id="input-email"
                                className="form-control form-control-alternative"
                                placeholder="Email"
                                value={user.email}
                                readOnly
                              />
                            </div>
                          </div>
                          {/* SĐT */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label"
                                htmlFor="input-phone"
                              >
                                Số điện thoại
                              </label>
                              <input
                                type="text"
                                id={`input-phone-${user.id}`}
                                className="form-control form-control-alternative"
                                placeholder="Số điện thoại"
                                value={user.phone}
                                onChange={(e) =>
                                  this.handleUpdate(
                                    user.id,
                                    "phone",
                                    e.target.value
                                  )
                                }
                              />
                            </div>
                          </div>
                        </div>

                        <div className="row">
                          {/* Họ và tên đệm */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label"
                                htmlFor="input-username"
                              >
                                Họ và tên đệm
                              </label>
                              <input
                                type="text"
                                id="input-firstName"
                                className="form-control form-control-alternative"
                                placeholder="Họ và tên"
                                value={user.firstName}
                              />
                            </div>
                          </div>
                          {/* Tên */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label"
                                htmlFor="input-username"
                              >
                                Tên
                              </label>
                              <input
                                type="text"
                                id="input-lastName"
                                className="form-control form-control-alternative"
                                placeholder="Họ và tên"
                                value={user.lastName}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="row">
                          {/* Giới tính */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label"
                                htmlFor="input-gender"
                              >
                                Giới tính
                              </label>
                              <input
                                type="text"
                                id="input-gender"
                                className="form-control form-control-alternative"
                                placeholder="Giới tính"
                                value={user.gender}
                              />
                            </div>
                          </div>
                          {/* ngày sinh */}
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label"
                                htmlFor="input-dob"
                              >
                                Ngày sinh
                              </label>
                              <input
                                type="text"
                                id="input-dob"
                                className="form-control form-control-alternative"
                                placeholder="Số điện thoại"
                                value={user.dob}
                              />
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* Thông tin liên lạc */}
                      <h6 className="heading-medium text-muted mb-4">
                        THÔNG TIN LIÊN LẠC
                      </h6>
                      <div className="pl-lg-4">
                        <div className="row">
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label"
                                htmlFor="input-username"
                              >
                                Họ và tên (Bố/Mẹ)
                              </label>
                              <input
                                type="text"
                                id="input-parentName"
                                className="form-control form-control-alternative"
                                placeholder="Họ và tên (Bố/Mẹ)"
                                value={user.parentName}
                              />
                            </div>
                          </div>
                          <div className="col-lg-6">
                            <div className="form-group text-align-left focused">
                              <label
                                className="form-control-label"
                                htmlFor="input-phone"
                              >
                                Số điện thoại
                              </label>
                              <input
                                type="text"
                                id="input-phone"
                                className="form-control form-control-alternative"
                                placeholder="Số điện thoại"
                                value={user.parentPhone}
                              />
                            </div>
                          </div>
                          <div className="col text-right">
                            <button
                              className="btn btn-sm btn-primary"
                              onClick={this.handleUpdate}
                            >
                              Cập nhật
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <br />
              {/* BẢNG THỐNG KÊ */}
              <div className="col-xl-8 order-xl-2">
                <div className="card bg-secondary shadow">
                  <div className="card-header bg-white border-0">
                    <div className="row align-items-center">
                      <div className="col">
                        <h3 className="mb-0 text-left heading">
                          TỔNG HỢP KẾT QUẢ HỌC TẬP
                        </h3>
                      </div>
                    </div>
                  </div>

                  {/* INCOME TABLE */}
                  {/* <div className="card-body">
                    <form>
                      <h6 className="heading-medium text-muted mb-4">
                        DOANH THU
                      </h6>
                      <div className="pl-lg-4">
                        <div>
                          <IncomeTable />
                        </div>
                      </div>
                    </form>
                  </div> */}

                  {/* OLYMPIC TABLE */}
                  <div className="card-body">
                    <form>
                      {/* Tham gia Olympic */}
                      <h6 className="heading-medium text-muted mb-4">
                        THAM GIA OLYMPIC
                      </h6>
                      <div className="pl-lg-4">
                        <div>
                          <OlympicTable />
                        </div>
                      </div>
                    </form>
                  </div>

                  {/* STUDY RESULT TABLE */}
                  <div className="card-body">
                    <form>
                      {/* Kết quả học tập */}
                      <h6 className="heading-medium text-muted mb-4">
                        KẾT QUẢ HỌC TẬP
                      </h6>
                      <div className="pl-lg-4">
                        <div>
                          <ResultTable />
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default UserProfile;
