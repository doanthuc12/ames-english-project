import { Table, Button } from "antd";
import Style from "./IncomeTable.module.css";
import userData from "../../../../data/users.json";

import { GrDocumentText } from "react-icons/gr";
import { AiOutlineFileSearch } from "react-icons/ai";

const columns = [
  {
    title: "Khoá học",
    className: "column-course",
    dataIndex: "course",
    align: "center",
  },
  {
    title: "Ngày bắt đầu",
    className: "column-date",
    dataIndex: "fromDate",
    align: "center",
    render: (text) => {
      // Format the date as "dd/mm/yyyy"
      const date = new Date(text);
      const formattedDate = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`;
      return formattedDate;
    },
  },
  {
    title: "Ngày kết thúc",
    className: "column-date",
    dataIndex: "toDate",
    align: "center",
    render: (text) => {
      // Format the date as "dd/mm/yyyy"
      const date = new Date(text);
      const formattedDate = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`;
      return formattedDate;
    },
  },

  {
    title: "Doanh Thu (VND)",
    className: "column-money",
    dataIndex: "money",
    align: "center",
    render: (text) => {
      // Handle cases where text is undefined or not in the expected format
      if (!text || typeof text !== "string") {
        return "N/A"; // Display "N/A" for undefined or improperly formatted values
      }

      // Convert the money string to a number for aggregation
      const money = parseFloat(text.replace(/[^0-9.-]+/g, ""));
      return money.toLocaleString("vi-VN", {
        style: "currency",
        currency: "VND",
      });
    },
  },
  {
    title: "File báo cáo",
    dataIndex: "link",
    align: "center",
    render: (text) => (
      <Button type="link" href={text} target="_blank">
        <AiOutlineFileSearch />
      </Button>
    ),
  },
  // {
  //   title: "Tổng Doanh Thu (VND)",
  //   className: "column-total-money",
  //   dataIndex: "totalMoney",
  //   align: "center",
  //   render: (text) => {
  //     // Handle cases where text is undefined or not in the expected format
  //     if (!text || typeof text !== "string") {
  //       return "N/A"; // Display "N/A" for undefined or improperly formatted values
  //     }

  //     // Convert the total money string to a number for formatting
  //     const totalMoney = parseFloat(text.replace(/[^0-9.-]+/g, ""));
  //     return totalMoney.toLocaleString("vi-VN", {
  //       style: "currency",
  //       currency: "VND",
  //     });
  //   },
  // },
];

// Define the user ID you want to retrieve
const targetUserId = 1; // Replace with the desired user ID

// Find the user object with the matching ID
const targetUser = userData.find((user) => user.id === targetUserId);

// Extract the revenue data from the target user
const revenueData = targetUser ? targetUser.revenue : [];

// Calculate the total revenue
const totalRevenue = revenueData.reduce((total, revenueItem) => {
  // Handle cases where money is undefined or not in the expected format
  if (!revenueItem.money || typeof revenueItem.money !== "string") {
    return total;
  }

  const money = parseFloat(revenueItem.money.replace(/[^0-9.-]+/g, ""));
  return total + money;
}, 0);

// Add the total revenue to the first row
if (revenueData.length > 0) {
  revenueData[0].totalMoney = totalRevenue.toLocaleString("vi-VN", {
    style: "currency",
    currency: "VND",
  });
}

const IncomeTable = () => (
  <Table
    className={Style.table}
    columns={columns}
    dataSource={revenueData}
    bordered
    footer={() => (
      <div>
        <b>Tổng doanh thu:</b>{" "}
        {totalRevenue.toLocaleString("vi-VN", {
          style: "currency",
          currency: "VND",
        })}
      </div>
    )}
  />
);

export default IncomeTable;
