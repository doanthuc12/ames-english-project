import React, { useEffect, useState } from "react";
import axios from "axios";

import { Table, Button } from "antd";
import Style from "./OlympicTable.module.css";
import userData from "../../../../data/users.json";

import { GrDocumentText } from "react-icons/gr";
import { AiOutlineFileSearch } from "react-icons/ai";

const cdnServerUrl = "https://server.sachso.edu.vn/api/v1.0/dynamic/query";
const authorizationKey = "Basic 12C1F7EF9AC8E288FBC2177B7F54D";

const columns = [
  {
    title: "Ngày tham gia",
    className: "column-date",
    dataIndex: "joinDate",
    align: "center",
    render: (text) => {
      // Format the date as "dd/mm/yyyy"
      const date = new Date(text);
      const formattedDate = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`;
      return formattedDate;
    },
  },
  {
    title: "Cuộc thi (HSSV/CBT)",
    className: "column-competition",
    dataIndex: "contestType",
    align: "center",
  },
  {
    title: "Thành tích",
    className: "column-achievement",
    dataIndex: "mark",
    align: "center",
  },
  {
    title: "Giấy chứng nhận",
    dataIndex: "link",
    align: "center",
    render: (text) => (
      <Button type="link" href={text} target="_blank">
        <AiOutlineFileSearch />
      </Button>
    ),
  },
];

const OlympicTable = () => {
  const [olympicData, setOlympicData] = useState([]); // State để lưu dữ liệu cho bảng olympic
  console.log("🚀 ~ olympicData:", olympicData);

  useEffect(() => {
    axios({
      headers: {
        "Content-Type": "application/json",
        Authorization: authorizationKey,
        ApplicationName: "Ebm",
      },
      method: "post",
      url: cdnServerUrl,
      data: {
        sqlCommand: "p_EBM_Student_Profile_Get_v2",
        parameters: {
          // StudentId: 11544998,
          StudentId: 11979755,
        },
      },
    })
      .then((response) => {
        // Set the users state with the data
        setOlympicData(response.data.results[0].olympic); // Assuming the data structure matches your expectations
      })
      .catch((error) => {
        console.log("error", error);
      });
  }, []);

  return (
    <Table
      className={Style.table}
      columns={columns}
      dataSource={olympicData}
      bordered
      // footer={() => (
      //   <div>
      //     <b>Kết quả học tập</b>{" "}
      //     {totalRevenue.toLocaleString("vi-VN", {
      //       style: "currency",
      //       currency: "VND",
      //     })}
      //   </div>
      // )}
    />
  );
};

export default OlympicTable;
