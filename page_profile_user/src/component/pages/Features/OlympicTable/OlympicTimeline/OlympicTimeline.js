import React, { useEffect, useState } from "react";
import axios from "axios";

import { Table, Button } from "antd";
import "./OlympicTimeline.css";

// import userData from "../../../../data/users.json";

import { GrDocumentText } from "react-icons/gr";
import { AiOutlineFileSearch } from "react-icons/ai";

const cdnServerUrl = "https://server.sachso.edu.vn/api/v1.0/dynamic/query";
const authorizationKey = "Basic 12C1F7EF9AC8E288FBC2177B7F54D";

function OlympicTimeline() {
  useEffect(() => {
    // Load Typekit asynchronously
    (function (d) {
      var tk = d.createElement("script");
      tk.src = "https://use.typekit.net/bkt6ydm.js";
      tk.async = true;
      tk.onload = function () {
        try {
          window.Typekit.load({ async: true });
        } catch (e) {}
      };
      d.head.appendChild(tk);
    })(document);
  }, []);

  return (
    <div>
      <header className="example-header">
        <h1 className="text-center">Simple Responsive Timeline</h1>
        <p>
          Handcrafted by{" "}
          <a
            href="http://overflowdg.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Overflow
          </a>
        </p>
      </header>
      <div className="container-fluid">
        {/* Basic Timeline */}
        <div className="row example-basic">
          <div className="col-md-12 example-title">
            <h2>Basic Timeline</h2>
            <p>Extra small devices (phones, less than 768px)</p>
          </div>
          <div className="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
            <ul className="timeline">{/* Timeline items go here */}</ul>
          </div>
        </div>

        {/* Split Timeline */}
        <div className="row example-split">
          <div className="col-md-12 example-title">
            <h2>Split Timeline</h2>
            <p>Small devices (tablets, 768px and up)</p>
          </div>
          <div className="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
            <ul className="timeline timeline-split">
              {/* Timeline items go here */}
            </ul>
          </div>
        </div>

        {/* Centered Timeline */}
        <div className="row example-centered">
          <div className="col-md-12 example-title">
            <h2>Centered Timeline</h2>
            <p>Medium devices (desktops, 992px and up).</p>
          </div>
          <div className="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
            <ul className="timeline timeline-centered">
              {/* Timeline items go here */}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default OlympicTimeline;
