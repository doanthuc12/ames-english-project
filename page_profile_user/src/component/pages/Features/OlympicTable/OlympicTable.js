import { Table, Button } from "antd";
import Style from "./OlympicTable.module.css";
import userData from "../../../../data/users.json";

import { GrDocumentText } from "react-icons/gr";
import { AiOutlineFileSearch } from "react-icons/ai";

const columns = [
  {
    title: "Ngày tham gia",
    className: "column-date",
    dataIndex: "date",
    align: "center",
    render: (text) => {
      // Format the date as "dd/mm/yyyy"
      const date = new Date(text);
      const formattedDate = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`;
      return formattedDate;
    },
  },
  {
    title: "Cuộc thi (HSSV/CBT)",
    className: "column-competition",
    dataIndex: "competition",
    align: "center",
  },
  {
    title: "Thành tích",
    className: "column-achievement",
    dataIndex: "achievement",
    align: "center",
  },
  {
    title: "Chưa rõ",
    dataIndex: "link",
    align: "center",
    render: (text) => (
      <Button type="link" href={text} target="_blank">
        <AiOutlineFileSearch />
      </Button>
    ),
  },
];

// Define the user ID you want to retrieve
const targetUserId = 1; // Replace with the desired user ID

// Find the user object with the matching ID
const targetUser = userData.find((user) => user.id === targetUserId);

// Extract the revenue data from the target user
const olympicData = targetUser ? targetUser.olympic : [];

const OlympicTable = () => (
  <Table
    className={Style.table}
    columns={columns}
    dataSource={olympicData}
    bordered
    // footer={() => (
    //   <div>
    //     <b>Kết quả học tập</b>{" "}
    //     {totalRevenue.toLocaleString("vi-VN", {
    //       style: "currency",
    //       currency: "VND",
    //     })}
    //   </div>
    // )}
  />
);

export default OlympicTable;
