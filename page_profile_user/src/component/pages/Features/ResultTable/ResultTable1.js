import React, { useEffect, useState } from "react";
import { Table, Button } from "antd";
import Styles from "./ResultTable.module.css";
import userData from "../../../../data/users.json";
import { AiOutlineStar, AiFillStar } from "react-icons/ai";
import axios from "axios";

const columns = [
  // {
  //   title: "Ngày cập nhật (mới nhất)",
  //   className: "column-date",
  //   dataIndex: "date",
  //   align: "center",
  //   render: (text) => {
  //     if (!text) {
  //       return "Không có"; // Trả về văn bản "Trống" nếu ngày cập nhật rỗng
  //     }

  //     // Format the date as "dd/mm/yyyy"
  //     const date = new Date(text);
  //     const formattedDate = `${date.getDate()}/${
  //       date.getMonth() + 1
  //     }/${date.getFullYear()}`;
  //     return formattedDate;
  //   },
  // },
  {
    title: "Khoá học",
    className: "column-courseName",
    dataIndex: "course",
    align: "center",
    render: (text, record) => {
      // Render a link (anchor) for the course name with the course URL
      return (
        <a href={record.link} target="_blank" rel="noopener noreferrer">
          {text}
        </a>
      );
    },
  },
  {
    title: "Ngày bắt đầu",
    className: "column-fromDate",
    dataIndex: "fromDate",
    align: "center",
    render: (text) => {
      // Format the date as "dd/mm/yyyy"
      const date = new Date(text);
      const formattedDate = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`;
      return formattedDate;
    },
  },
  {
    title: "Ngày kết thúc",
    className: "column-toDate",
    dataIndex: "toDate",
    align: "center",
    render: (text) => {
      // Format the date as "dd/mm/yyyy"
      const date = new Date(text);
      const formattedDate = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`;
      return formattedDate;
    },
  },
  {
    title: "Trạng thái",
    className: "column-state",
    dataIndex: "state",
    align: "center",
  },
  {
    title: "Tiến độ (%)",
    className: "column-percentage",
    dataIndex: "percentage",
    align: "center",
  },
  {
    title: "Đánh giá",
    dataIndex: "percentage",
    align: "center",
    render: (percentage) => {
      // Create an array of 5 star icons with appropriate fill based on the percentage range
      const stars = [
        <AiOutlineStar key="star1" />,
        <AiOutlineStar key="star2" />,
        <AiOutlineStar key="star3" />,
        <AiOutlineStar key="star4" />,
        <AiOutlineStar key="star5" />,
      ];

      // Calculate the number of filled stars based on the percentage
      if (percentage >= 91) {
        stars.fill(
          <AiFillStar key="star5" className={Styles.yellow_star} />,
          0,
          5
        );
      } else if (percentage >= 71) {
        stars.fill(
          <AiFillStar key="star4" className={Styles.yellow_star} />,
          0,
          4
        );
      } else if (percentage >= 51) {
        stars.fill(
          <AiFillStar key="star3" className={Styles.yellow_star} />,
          0,
          3
        );
      } else if (percentage >= 21) {
        stars.fill(
          <AiFillStar key="star2" className={Styles.yellow_star} />,
          0,
          2
        );
      } else if (percentage >= 10) {
        stars.fill(
          <AiFillStar key="star2" className={Styles.yellow_star} />,
          0,
          1
        );
      } else {
        stars.fill(
          <AiFillStar key="star1" className={Styles.yellow_star} />,
          0,
          0
        );
      }

      return <div>{stars}</div>;
    },
  },
];
const cdnServerUrl = "https://server.sachso.edu.vn/api/v1.0/dynamic/query";
const authorizationKey = "Basic 12C1F7EF9AC8E288FBC2177B7F54D";

const ResultTable1 = () => {
  const [studyResult, setStudyResult] = useState([]); // State để lưu dữ liệu cho bảng ResultTable
  console.log("🚀 ~ studyResult:", studyResult);

  // useEffect(() => {
  //   // Gọi API để lấy dữ liệu cho bảng ResultTable
  //   axios
  //     .get("URL_API_CUA_BAN") // Thay URL_API_CUA_BAN bằng URL của API của bạn
  //     .then((response) => {
  //       // Lấy dữ liệu từ response và cập nhật state studyResultData
  //       setStudyResultData(response.data);
  //     })
  //     .catch((error) => {
  //       console.error("Error fetching data: ", error);
  //     });
  // }, []); // Sử dụng một lần khi component được render

  useEffect(() => {
    axios({
      headers: {
        "Content-Type": "application/json",
        Authorization: authorizationKey,
        ApplicationName: "Ebm",
      },
      method: "post",
      url: cdnServerUrl,
      data: {
        sqlCommand: "p_EBM_Student_Profile_Get_v2",
        parameters: {
          // StudentId: 11544998,
          StudentId: 11979755,
        },
      },
    })
      .then((response) => {
        // Set the users state with the data
        setStudyResult(response.data.results[0].revenue); // Assuming the data structure matches your expectations
      })
      .catch((error) => {
        console.log("error", error);
      });
  }, []);

  return (
    <Table
      className={Styles.table}
      columns={columns}
      dataSource={studyResult}
      bordered
    />
  );
};
export default ResultTable1;
