import { Table, Button, Empty } from "antd";
import Style from "./ResultTable.module.css";
import userData from "../../../../data/users.json";
import { AiOutlineStar, AiFillStar } from "react-icons/ai";

import Styles from "./ResultTable.module.css";

const columns = [
  // {
  //   title: "Ngày cập nhật (mới nhất)",
  //   className: "column-date",
  //   dataIndex: "date",
  //   align: "center",
  //   render: (text) => {
  //     if (!text) {
  //       return "Không có"; // Trả về văn bản "Trống" nếu ngày cập nhật rỗng
  //     }

  //     // Format the date as "dd/mm/yyyy"
  //     const date = new Date(text);
  //     const formattedDate = `${date.getDate()}/${
  //       date.getMonth() + 1
  //     }/${date.getFullYear()}`;
  //     return formattedDate;
  //   },
  // },
  {
    title: "Khoá học",
    className: "column-courseName",
    dataIndex: "course",
    align: "center",
    render: (text, record) => {
      // Render a link (anchor) for the course name with the course URL
      return (
        <a href={record.link} target="_blank" rel="noopener noreferrer">
          {text}
        </a>
      );
    },
  },
  {
    title: "Ngày bắt đầu",
    className: "column-fromDate",
    dataIndex: "fromDate",
    align: "center",
    render: (text) => {
      // Format the date as "dd/mm/yyyy"
      const date = new Date(text);
      const formattedDate = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`;
      return formattedDate;
    },
  },
  {
    title: "Ngày kết thúc",
    className: "column-toDate",
    dataIndex: "toDate",
    align: "center",
    render: (text) => {
      // Format the date as "dd/mm/yyyy"
      const date = new Date(text);
      const formattedDate = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`;
      return formattedDate;
    },
  },
  {
    title: "Trạng thái",
    className: "column-state",
    dataIndex: "state",
    align: "center",
  },
  {
    title: "Tiến độ (%)",
    className: "column-percentage",
    dataIndex: "percentage",
    align: "center",
  },
  {
    title: "Đánh giá",
    dataIndex: "percentage",
    align: "center",
    render: (percentage) => {
      // Create an array of 5 star icons with appropriate fill based on the percentage range
      const stars = [
        <AiOutlineStar key="star1" />,
        <AiOutlineStar key="star2" />,
        <AiOutlineStar key="star3" />,
        <AiOutlineStar key="star4" />,
        <AiOutlineStar key="star5" />,
      ];

      // Calculate the number of filled stars based on the percentage
      if (percentage >= 91) {
        stars.fill(
          <AiFillStar key="star5" className={Styles.yellow_star} />,
          0,
          5
        );
      } else if (percentage >= 71) {
        stars.fill(
          <AiFillStar key="star4" className={Styles.yellow_star} />,
          0,
          4
        );
      } else if (percentage >= 51) {
        stars.fill(
          <AiFillStar key="star3" className={Styles.yellow_star} />,
          0,
          3
        );
      } else if (percentage >= 21) {
        stars.fill(
          <AiFillStar key="star2" className={Styles.yellow_star} />,
          0,
          2
        );
      } else if (percentage >= 10) {
        stars.fill(
          <AiFillStar key="star2" className={Styles.yellow_star} />,
          0,
          1
        );
      } else {
        stars.fill(
          <AiFillStar key="star1" className={Styles.yellow_star} />,
          0,
          0
        );
      }

      return <div>{stars}</div>;
    },
  },
];

// Define the user ID you want to retrieve
const targetUserId = 1; // Replace with the desired user ID

// Find the user object with the matching ID
const targetUser = userData.find((users) => users.id === targetUserId);

// Extract the study results data from the target user
const studyResultData = targetUser ? targetUser.studyResults : [];

const ResultTable = () => {
  if (studyResultData.length === 0) {
    return <Empty description="Không có dữ liệu" />;
  }

  return (
    <Table
      className={Style.table}
      columns={columns}
      dataSource={studyResultData}
      bordered
    />
  );
};

export default ResultTable;
