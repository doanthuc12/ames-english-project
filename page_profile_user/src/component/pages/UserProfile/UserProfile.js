import React, { useState, useEffect } from "react";
import Style from "./UserProfile.module.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Avatar, Card } from "antd";
import IncomeTable from "../Features/IncomeTable/IncomeTable";

// import userData from "../../../data/users.json";

const UserProfile = () => {
  const [user, setUser] = useState({});

  useEffect(() => {
    // Trong useEffect, bạn có thể tải dữ liệu từ tệp JSON bên ngoài.
    // Đây là ví dụ giả lập việc tải dữ liệu từ tệp JSON.
    fetch("../../../data/users.json")
      .then((response) => response.json())
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        console.error("Error loading user data:", error);
      });
  }, []);

  return (
    <div className="container">
      <div className={Style.main_body}>
        <nav aria-label="breadcrumb" className="main-breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="index.html">Liên hệ</a>
            </li>
            <li className="breadcrumb-item">
              <a href="javascript:void(0)">Thông tin người dùng</a>
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              Thông tin cá nhân
            </li>
          </ol>
        </nav>
        <div className={Style.gutters_sm}>
          <div className="col-md-4 mb-3">
            <div className={Style.card}>
              <div className={Style.card_body}>
                <div className="d-flex flex-column align-items-center text-center">
                  <img
                    src={user.avatar}
                    alt="User Avatar"
                    className={Style.avatar}
                  />
                  <div className="mt-3">
                    <h4>{user.name}</h4>
                    <p className="text-secondary mb-1">Học viên</p>
                    <button className="btn btn-outline-primary">Liên hệ</button>
                  </div>
                </div>
              </div>
            </div>
            {/* <div className="card mt-3">
              <ul className="list-group list-group-flush">
                <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <h6 className="mb-0">
                    <i className="fa fa-globe mr-2"></i>Chưa rõ
                  </h6>
                  <span className="text-secondary">https://bootdey.com</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <h6 className="mb-0">
                    <i className="fa fa-github mr-2"></i>Chưa rõ
                  </h6>
                  <span className="text-secondary">bootdey</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <h6 className="mb-0">
                    <i className="fa fa-twitter mr-2"></i>Chưa rõ
                  </h6>
                  <span className="text-secondary">@bootdey</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <h6 className="mb-0">
                    <i className="fa fa-instagram mr-2"></i>Chưa rõ
                  </h6>
                  <span className="text-secondary">bootdey</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <h6 className="mb-0">
                    <i className="fa fa-facebook mr-2"></i>Chưa rõ
                  </h6>
                  <span className="text-secondary">bootdey</span>
                </li>
              </ul>
            </div> */}
          </div>
          <div className="col-md-8">
            <div className="card mb-3">
              <div className={Style.card_body}>
                <div className="row">
                  <div className="col-sm-3">
                    <h6 className="mb-0">Họ và tên</h6>
                  </div>
                  <div className="col-sm-9 text-secondary">{user.name}</div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-3">
                    <h6 className="mb-0">Giới tính</h6>
                  </div>
                  <div className="col-sm-9 text-secondary">{user.gender}</div>
                </div>

                <hr />
                <div className="row">
                  <div className="col-sm-3">
                    <h6 className="mb-0">Ngày sinh</h6>
                  </div>
                  <div className="col-sm-9 text-secondary">{user.dob}</div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-3">
                    <h6 className="mb-0">Email</h6>
                  </div>
                  <div className="col-sm-9 text-secondary">{user.email}</div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-3">
                    <h6 className="mb-0">Số điện thoại</h6>
                  </div>
                  <div className="col-sm-9 text-secondary">{user.phone}</div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-3">
                    <h6 className="mb-0">Địa chỉ</h6>
                  </div>
                  <div className="col-sm-9 text-secondary">{user.address}</div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-12">
                    <a
                      className="btn btn-info "
                      target="_blank"
                      href="https://www.bootdey.com/snippets/view/profile-edit-data-and-skills"
                    >
                      Edit
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className={Style.table}>
              <IncomeTable />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
